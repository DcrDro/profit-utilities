namespace Opacha.Profit.Extensions.Maths
{
    public static class MathExtensions
    {
        #region Clamping

        public static void MinClamp(this ref float value, float minValue)
        {
            if (value < minValue) value = minValue;
        }
        
        public static void MinClamp(this ref int value, int minValue)
        {
            if (value < minValue) value = minValue;
        }
        
        public static void MaxClamp(this ref float value, float maxValue)
        {
            if (value > maxValue) value = maxValue;
        }

        public static void MaxClamp(this ref int value, int maxValue)
        {
            if (value > maxValue) value = maxValue;
        }
        
        public static void Clamp(this ref float value, float minValue, float maxValue)
        {
            if (value < minValue) value = minValue;
            else if (value > maxValue) value = maxValue;
        }
        
        public static void Clamp(this ref int value, int minValue, int maxValue)
        {
            if (value < minValue) value = minValue;
            else if (value > maxValue) value = maxValue;
        }

        #endregion

        #region Average

        public static float Average(params int[] values)
        {
            int sum = 0;
            foreach (var value in values)
            {
                sum += value;
            }

            return (float) sum / values.Length;
        }

        public static float Average(params float[] values)
        {
            float sum = 0;
            foreach (var value in values)
            {
                sum += value;
            }

            return sum / values.Length;
        }
        
        #endregion
    }
}